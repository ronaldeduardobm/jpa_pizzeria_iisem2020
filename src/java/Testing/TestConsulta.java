/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testing;

import DAO.Conexion;
import DAO.PizzaJpaController;
import DTO.Pizza;
import java.util.List;

/**
 *
 * @author madar
 */
public class TestConsulta {
    
    public static void main(String[] args) {
        Conexion con=Conexion.getConexion();
        PizzaJpaController pizzaDAO=new PizzaJpaController(con.getBd());
        List<Pizza> pizzas=pizzaDAO.findPizzaEntities();
        for(Pizza p:pizzas)
        {
            System.out.println("Pizza:"+p.getIdPizza()+",tamaño:"+p.getIdTipo().getDescripción()+",Sabor:"+p.getIdSabor().getDescripcion()+",valor:"+p.getValor());
        }
        
    }
    
}
