/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DAO.exceptions.PreexistingEntityException;
import DTO.IngredienteAdicional;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DTO.PizzaAdicional;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author madar
 */
public class IngredienteAdicionalJpaController implements Serializable {

    public IngredienteAdicionalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(IngredienteAdicional ingredienteAdicional) throws PreexistingEntityException, Exception {
        if (ingredienteAdicional.getPizzaAdicionalList() == null) {
            ingredienteAdicional.setPizzaAdicionalList(new ArrayList<PizzaAdicional>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<PizzaAdicional> attachedPizzaAdicionalList = new ArrayList<PizzaAdicional>();
            for (PizzaAdicional pizzaAdicionalListPizzaAdicionalToAttach : ingredienteAdicional.getPizzaAdicionalList()) {
                pizzaAdicionalListPizzaAdicionalToAttach = em.getReference(pizzaAdicionalListPizzaAdicionalToAttach.getClass(), pizzaAdicionalListPizzaAdicionalToAttach.getIdPizza());
                attachedPizzaAdicionalList.add(pizzaAdicionalListPizzaAdicionalToAttach);
            }
            ingredienteAdicional.setPizzaAdicionalList(attachedPizzaAdicionalList);
            em.persist(ingredienteAdicional);
            for (PizzaAdicional pizzaAdicionalListPizzaAdicional : ingredienteAdicional.getPizzaAdicionalList()) {
                IngredienteAdicional oldIdIngredienteOfPizzaAdicionalListPizzaAdicional = pizzaAdicionalListPizzaAdicional.getIdIngrediente();
                pizzaAdicionalListPizzaAdicional.setIdIngrediente(ingredienteAdicional);
                pizzaAdicionalListPizzaAdicional = em.merge(pizzaAdicionalListPizzaAdicional);
                if (oldIdIngredienteOfPizzaAdicionalListPizzaAdicional != null) {
                    oldIdIngredienteOfPizzaAdicionalListPizzaAdicional.getPizzaAdicionalList().remove(pizzaAdicionalListPizzaAdicional);
                    oldIdIngredienteOfPizzaAdicionalListPizzaAdicional = em.merge(oldIdIngredienteOfPizzaAdicionalListPizzaAdicional);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findIngredienteAdicional(ingredienteAdicional.getIdIngrediente()) != null) {
                throw new PreexistingEntityException("IngredienteAdicional " + ingredienteAdicional + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(IngredienteAdicional ingredienteAdicional) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            IngredienteAdicional persistentIngredienteAdicional = em.find(IngredienteAdicional.class, ingredienteAdicional.getIdIngrediente());
            List<PizzaAdicional> pizzaAdicionalListOld = persistentIngredienteAdicional.getPizzaAdicionalList();
            List<PizzaAdicional> pizzaAdicionalListNew = ingredienteAdicional.getPizzaAdicionalList();
            List<String> illegalOrphanMessages = null;
            for (PizzaAdicional pizzaAdicionalListOldPizzaAdicional : pizzaAdicionalListOld) {
                if (!pizzaAdicionalListNew.contains(pizzaAdicionalListOldPizzaAdicional)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PizzaAdicional " + pizzaAdicionalListOldPizzaAdicional + " since its idIngrediente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<PizzaAdicional> attachedPizzaAdicionalListNew = new ArrayList<PizzaAdicional>();
            for (PizzaAdicional pizzaAdicionalListNewPizzaAdicionalToAttach : pizzaAdicionalListNew) {
                pizzaAdicionalListNewPizzaAdicionalToAttach = em.getReference(pizzaAdicionalListNewPizzaAdicionalToAttach.getClass(), pizzaAdicionalListNewPizzaAdicionalToAttach.getIdPizza());
                attachedPizzaAdicionalListNew.add(pizzaAdicionalListNewPizzaAdicionalToAttach);
            }
            pizzaAdicionalListNew = attachedPizzaAdicionalListNew;
            ingredienteAdicional.setPizzaAdicionalList(pizzaAdicionalListNew);
            ingredienteAdicional = em.merge(ingredienteAdicional);
            for (PizzaAdicional pizzaAdicionalListNewPizzaAdicional : pizzaAdicionalListNew) {
                if (!pizzaAdicionalListOld.contains(pizzaAdicionalListNewPizzaAdicional)) {
                    IngredienteAdicional oldIdIngredienteOfPizzaAdicionalListNewPizzaAdicional = pizzaAdicionalListNewPizzaAdicional.getIdIngrediente();
                    pizzaAdicionalListNewPizzaAdicional.setIdIngrediente(ingredienteAdicional);
                    pizzaAdicionalListNewPizzaAdicional = em.merge(pizzaAdicionalListNewPizzaAdicional);
                    if (oldIdIngredienteOfPizzaAdicionalListNewPizzaAdicional != null && !oldIdIngredienteOfPizzaAdicionalListNewPizzaAdicional.equals(ingredienteAdicional)) {
                        oldIdIngredienteOfPizzaAdicionalListNewPizzaAdicional.getPizzaAdicionalList().remove(pizzaAdicionalListNewPizzaAdicional);
                        oldIdIngredienteOfPizzaAdicionalListNewPizzaAdicional = em.merge(oldIdIngredienteOfPizzaAdicionalListNewPizzaAdicional);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ingredienteAdicional.getIdIngrediente();
                if (findIngredienteAdicional(id) == null) {
                    throw new NonexistentEntityException("The ingredienteAdicional with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            IngredienteAdicional ingredienteAdicional;
            try {
                ingredienteAdicional = em.getReference(IngredienteAdicional.class, id);
                ingredienteAdicional.getIdIngrediente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingredienteAdicional with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PizzaAdicional> pizzaAdicionalListOrphanCheck = ingredienteAdicional.getPizzaAdicionalList();
            for (PizzaAdicional pizzaAdicionalListOrphanCheckPizzaAdicional : pizzaAdicionalListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This IngredienteAdicional (" + ingredienteAdicional + ") cannot be destroyed since the PizzaAdicional " + pizzaAdicionalListOrphanCheckPizzaAdicional + " in its pizzaAdicionalList field has a non-nullable idIngrediente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(ingredienteAdicional);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<IngredienteAdicional> findIngredienteAdicionalEntities() {
        return findIngredienteAdicionalEntities(true, -1, -1);
    }

    public List<IngredienteAdicional> findIngredienteAdicionalEntities(int maxResults, int firstResult) {
        return findIngredienteAdicionalEntities(false, maxResults, firstResult);
    }

    private List<IngredienteAdicional> findIngredienteAdicionalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(IngredienteAdicional.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public IngredienteAdicional findIngredienteAdicional(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(IngredienteAdicional.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngredienteAdicionalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<IngredienteAdicional> rt = cq.from(IngredienteAdicional.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
